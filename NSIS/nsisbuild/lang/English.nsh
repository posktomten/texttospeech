; Language ID
; Enlish (USA) 1033

LangString UNINST_EXCLUDE_ERROR 1033 "Error creating an exclusion list."
LangString UNINST_DATA_ERROR 1033 "Error creating the uninstaller data: $\r$\nCannot find an exclusion list."
LangString UNINST_DAT_NOT_FOUND 1033 "$UNINST_DAT not found, unable to perform uninstall. Manually delete files."
LangString UNINST_DAT_MISSING 1033 "$UNINST_DAT is missing, some elements could not be removed. These can be removed manually."
LangString UNINST_DEL_FILE 1033 "Delete File"
LangString UNINSTALL 1033 "Uninstall"
LangString NEXT_CD 1033 "Please insert the disk labeled"
LangString CD_NOT_FOUND 1033 "Can not find the file $0.$\r$\nPlease insert the correct disk."
LangString EXTRACT 1033 "Decompression:"
LangString REMOVEALL 1033 "Remove all files and folders from '$INSTDIR'?"
LangString UNCOMPLATE 1033 "$(^Name) uninstall complete."

LangString DESC_Sec1 1033 "Install Qtexttospeech and all necessary library files. A program for turning text into speech."
;LangString DESC_Sec2 1033 "FFmpeg is a free and open-source software project consisting of a suite of libraries and programs for handling video, audio, and other multimedia files and streams."
;LangString DESC_Sec3 1033 "Qt is a cross-platform software for creating graphical user interfaces as well as cross-platform applications."

LangString INST_TYPE_FULL 1033 "Full"
LangString INST_TYPE_MINIMAL 1033 "Minimal"
LangString INST_TYPE_CUSTOM 1033 "Custom"

LangString WELCOME 1033 "Welcome to Qtexttospeech 1.1.0 Setup"
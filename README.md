# Library, and a program to test the library with. Uses synthetic speech.
# Download and try [Wiki page](https://gitlab.com/posktomten/texttospeech/-/wikis/home)
# This project is under development.

The program uses these libraries:<br>
- [download_install](https://gitlab.com/posktomten/download_offline_installer)<br>
- [libcheckforupdates](https://gitlab.com/posktomten/libcheckforupdates)<br>
- [libzsyncupdateappimage](https://gitlab.com/posktomten/libzsyncupdateappimage)<br>
- [libcreateshortcut, Windows](https://gitlab.com/posktomten/libcreateshortcut_windows)<br>
- [libcreateshortcut, Linux](https://gitlab.com/posktomten/libcreateshortcut)<br>


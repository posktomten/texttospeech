<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Libtexttospeech</name>
    <message>
        <location filename="../texttospeech.cpp" line="89"/>
        <source>Ready</source>
        <translation>Berädd</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="91"/>
        <source>Speaking</source>
        <translation>Talar</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="93"/>
        <source>Paused</source>
        <translation>Pausad</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="96"/>
        <source>BackendError (No working speech synthesis in the operating system)</source>
        <translation>BackendError (Ingen fungerande talsyntes i operativsystemet)</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="98"/>
        <source>Unknown error</source>
        <translation>Okänt fel</translation>
    </message>
</context>
</TS>

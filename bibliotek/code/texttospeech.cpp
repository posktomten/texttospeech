// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          texttospeech
//          Copyright (C) 2022 -2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This library is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "texttospeech.h"
#include "texttospeech_global.h"
Libtexttospeech::Libtexttospeech()
{
    speech = new QTextToSpeech;
    speech->setVolume(1.0f);
    connect(speech, &QTextToSpeech::stateChanged, this, &Libtexttospeech::getStateHasChenged);
}
Libtexttospeech::~Libtexttospeech()
{
}
// SET TEXT
void Libtexttospeech::setText(const QString *text)
{
    speech->say(*text);
}
// SET PITCH
void Libtexttospeech::setPitch(const int inputPitch)
{
    double _inputPitch = (double) inputPitch;
    _inputPitch = _inputPitch / 10;
    speech->setPitch(_inputPitch);
}
// RATE
void Libtexttospeech::setRate(const int inputRate)
{
    double _inputRate = (double) inputRate;
    _inputRate = _inputRate / 10;
    speech->setRate(_inputRate);
}
// LOCALE
void Libtexttospeech::setLocale(QLocale locale)
{
    speech->setLocale(locale);
}
void Libtexttospeech::setVoice(QVoice voice)
{
    speech->setVoice(voice);
}
// Qt6 only
//void Libtexttospeech::setEngine(QTextToSpeech endgine)
//{
//    speech->setEngine(endgine);
//}

void Libtexttospeech::setVolume(const int inputVolume)
{
    double _inputVolume = (double) inputVolume;
    _inputVolume = _inputVolume / 10;
    speech->setVolume(_inputVolume);
}
void Libtexttospeech::setStop()
{
    speech->stop();
}
void Libtexttospeech::setPause()
{
    speech->pause();
}
void Libtexttospeech::setResume()
{
    speech->resume();
}

// public slots:
void Libtexttospeech::getStateHasChenged(QTextToSpeech::State state)
{
    QString *_state = new QString;

    if(state == QTextToSpeech::Ready) {
        *_state = QString(tr("Ready"));
    } else if(state == QTextToSpeech::Speaking) {
        *_state = QString(tr("Speaking"));
    } else if(state == QTextToSpeech::Paused) {
        *_state = QString(tr("Paused"));
        // BackendError, NOT in Qt6, Qt5 only
//    } else if(state == QTextToSpeech::BackendError) {
        *_state = QString(tr("BackendError (No working speech synthesis in the operating system)"));
    } else {
        *_state = QString(tr("Unknown error"));
    }

    emit stateHasChenged(_state);
}

QVector<QLocale> Libtexttospeech::getLocales()
{
    QVector<QLocale> locales = speech->availableLocales();
    return locales;
}
QVector<QVoice> Libtexttospeech::getVoices()
{
    QVector<QVoice> voices = speech->availableVoices();
    return voices;
}
void Libtexttospeech::finish()
{
    delete speech;
}

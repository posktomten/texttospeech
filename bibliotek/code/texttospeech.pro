#// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          texttospeech
#//          Copyright (C) 2022 -2023 Ingemar Ceicer
#//          https://gitlab.com/posktomten/texttospeech
#//          programmering1@ceicer.com
#//
#//   This library is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License as published by
#//   the Free Software Foundation version 3.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

QT -= gui
QT += texttospeech core
TEMPLATE = lib
DEFINES += LIBTEXTTOSPEECH_LIBRARY
#win32:CONFIG+=staticlib
CONFIG+=staticlib
#CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    texttospeech.cpp

HEADERS += \
    texttospeech_global.h \
    texttospeech.h

TRANSLATIONS += \
    i18n/_texttosspech_lib_sv_SE.ts \
    i18n/_texttosspech_lib_template_xx_XX.ts


CONFIG(release, debug|release):BUILD=RELEASE
CONFIG(debug, debug|release):BUILD=DEBUG


equals(BUILD,RELEASE) {

    TARGET=texttospeech

}
equals(BUILD,DEBUG) {

    TARGET=texttospeechd
}



equals(QT_MAJOR_VERSION, 5) {
DESTDIR=$$OUT_PWD-lib
#unix:DESTDIR=/home/ingemar/PROGRAMMERING/libtexttospeech/testprogram/lib5
#win32:DESTDIR=C:\Users\posktomten\PROGRAMMERING\libtexttospeech\testprogram\lib5
}

#Qt6.4.x which I have not tested
equals(QT_MAJOR_VERSION, 6) {
DESTDIR=$$OUT_PWD-lib
#unix:DESTDIR=/home/ingemar/PROGRAMMERING/libtexttospeech/testprogram/lib6
#win32:DESTDIR=C:\Users\posktomten\PROGRAMMERING\libtexttospeech\testprogram\lib6
}

message (--------------------------------------------------)
message (OS: $$QMAKE_HOST.os)
message (Arch: $$QMAKE_HOST.arch)
message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (qmake path: $$QMAKE_QMAKE)
message (Qt version: $$QT_VERSION)
message(*.pro path: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(PWD: $$PWD)
message(--------------------------------------------------)

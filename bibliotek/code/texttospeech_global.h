// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          texttospeech
//          Copyright (C) 2022 -2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This library is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#if !defined(LIBTEXTTOSPEECH_GLOBALH)
#include <QtCore/qglobal.h>
#define LIBTEXTTOSPEECH_GLOBALH

#if defined(LIBTEXTTOSPEECH_LIBRARY)
#define LIBTEXTTOSPEECH_EXPORT Q_DECL_EXPORT
#else
#  define LIBTEXTTOSPEECH_EXPORT Q_DECL_IMPORT
#endif

#endif // LIBTEXTTOSPEECH_GLOBALH

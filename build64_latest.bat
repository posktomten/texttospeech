ECHO OFF
CLS
:MENU
ECHO.
ECHO   PRESS 1, 2 OR 3 to select your task, or 34 to EXIT.
ECHO.
ECHO   1 - Build Qt5.15.2
ECHO   2 - Build Qt5.15.15
ECHO   3 - Build Qt6.7.3
ECHO   4 - EXIT
ECHO.
SET /P M=Type 1, 2, 3, 4 or 5 then press ENTER: 
IF %M%==1 GOTO Qt5.15.2
IF %M%==2 GOTO Qt5.15.15
IF %M%==4 GOTO Qt6.7.3
IF %M%==5 GOTO EOF

:Qt5.15.2
set QTVERSION="C:/Qt/5.15.2/mingw81_64/bin/qmake.exe"
set "LIB=5.15.2"
GOTO KEEPON
:Qt5.15.15
set QTVERSION="C:/Qt/5.15.15/mingw_64/bin/qmake.exe"
set "LIB=5.15.15"
GOTO KEEPON
:Qt6.7.3
set QTVERSION="C:/Qt/6.7.3/mingw_64/bin/qmake.exe"
set "LIB=6.7.3"
GOTO KEEPON

C:\Qt\5.15.2\mingw81_64\bin


:KEEPON
set "COMPILER=C:\Qt\Tools\mingw1120_64\bin\mingw32-make.exe"

mkdir build
cd build

    %QTVERSION% -project ../code/*.pro 
	%QTVERSION% "CONFIG+=debug" ../code/*.pro -spec win32-g++ "CONFIG+=qtquickcompiler" && %COMPILER% qmake_all
  
    %COMPILER% -j6
    %COMPILER% clean
cd ..

rmdir /S /Q build
mkdir lib_Qt-%QTVERSION%_MinGW
copy build-lib\*.* lib_Qt-%QTVERSION%_MinGW
rmdir /S /Q build-lib

mkdir build
cd build
    %QTVERSION% -project ../code/selectfont.pro 
	%QTVERSION% "CONFIG+=release" ../code/selectfont.pro -spec win32-g++ "CONFIG+=qtquickcompiler" && %COMPILER% qmake_all
  
    %COMPILER% -j6
    %COMPILER% clean
cd ..
rmdir /S /Q build
mkdir lib_Qt-%LIB%_MinGW
copy build-lib\*.* lib_Qt-%LIB%_MinGW
rmdir /S /Q build-lib


:EOF


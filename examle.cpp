#include "widget.h"
#include "ui_widget.h"
// https://www.ics.com/blog/text-speech-look-qt-speech-module
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(ui->pbTextToSpeech, &QPushButton::clicked, []()->void{

        // List the available engines.
        QStringList engines = QTextToSpeech::availableEngines();
        qDebug() << "Available engines:";

        for(auto engine : engines)
        {
            qDebug() << "  " << engine;
        }
        // Create an instance using the default engine/plugin.
        QTextToSpeech *speech = new QTextToSpeech();
        // List the available locales.
        qDebug() << "Available locales:";

        for(auto locale : speech->availableLocales())
        {
            qDebug() << "  " << locale;
        }
        // Set locale.
        speech->setLocale(QLocale(QLocale::English, QLocale::LatinScript, QLocale::UnitedStates));
        // List the available voices.
        qDebug() << "Available voices:";

        for(auto voice : speech->availableVoices())
        {
            qDebug() << "  " << voice.name();
        }
        // Display properties.
        qDebug() << "Locale:" << speech->locale();
        qDebug() << "Pitch:" << speech->pitch();
        qDebug() << "Rate:" << speech->rate();
        qDebug() << "Voice:" << speech->voice().name();
        qDebug() << "Volume:" << speech->volume();
        qDebug() << "State:" << speech->state();

        // Say something.
        const QString notIdentical = QStringLiteral("The hash sums are NOT identical");
        const QString identical = QStringLiteral("The hash sums are identical");
        speech->say(notIdentical);
        // Wait for sound to play before exiting.
//        QThread::sleep(10);
    });
}
Widget::~Widget()
{
    delete ui;
}

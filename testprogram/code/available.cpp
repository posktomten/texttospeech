// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Qtexttospeech
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "dialog.h"
#include "ui_dialog.h"

void Dialog::available()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // ENGINES
    avaiableEngines = QTextToSpeech::availableEngines();
    ui->comEngines->addItems(avaiableEngines);
    //
    // LOCALE
    avaiableLocal = mLibtexttospeech.getLocales();
    QStringList nativeLocales;

    foreach(QLocale l, avaiableLocal) {
        QString locale = l.nativeTerritoryName();
        // QString locale = l.nativeCountryName();
        ui->comLocale->addItem(locale);
        nativeLocales << locale;
    }

    settings.beginGroup("Settings");
    QString savedlocale = settings.value("savedlocale").toString();
    settings.endGroup();

    if(nativeLocales.contains(savedlocale)) {
        ui->comLocale->setCurrentText(savedlocale);
    }

//    mLibtexttospeech.setLocale(avaiableLocal.at(0));
    //
    // VOICES
    avaiableVoices = mLibtexttospeech.getVoices();
    QStringList savedVoiceNames;

    foreach(QVoice v, avaiableVoices) {
        QString voice = v.name();
        ui->comVoices->addItem(voice);
        savedVoiceNames << voice;
    }

    settings.beginGroup("Settings");
    QString savedvoice = settings.value("savedvoice").toString();
    settings.endGroup();

    if(savedVoiceNames.contains(savedvoice)) {
        ui->comVoices->setCurrentText(savedvoice);
    }

    mLibtexttospeech.setVoice(avaiableVoices.at(ui->comVoices->currentIndex()));
}

void Dialog::dropEvent(QDropEvent * ev)
{
    QStringList localUrl;
    QList<QUrl> urls = ev->mimeData()->urls();

    foreach(QUrl u, urls) {
        localUrl << u.toLocalFile();
    }

    QFile file;

    foreach(QString s, localUrl) {
        file.setFileName(s);

        if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&file);

        while(!in.atEnd()) {
            QString line = in.readLine();
            ui->plainTextEdit->appendPlainText(line);
        }
    }

    file.close();

    if(ui->cheSpeak->isChecked()) {
        QString in = ui->plainTextEdit->toPlainText();
        QString *text = new QString(in);
        mLibtexttospeech.setText(text);
    }

    ev->acceptProposedAction();
}
void Dialog::about()
{
    QString ostype =  QSysInfo::prettyProductName();
    QString bit;

    if(QSysInfo::currentCpuArchitecture() == "x86_64") {
        bit = tr(". You are using a sixty-four bit operating system");
    } else {
        bit = tr(". You are using a thirty-two bit operating system");
    }

#ifdef Q_OS_LINUX
    QString *linux_str = new QString(tr("Except that this program may crash if you touch the volume control."));
#endif
#ifdef Q_OS_WINDOWS
    QString *linux_str = new QString("");
#endif
    QString *p = new QString(tr("I'm going to do my very best. Please write or paste the text you want me to read aloud. You can also drag and drop a text file to me. Then I will read it to you. I use the operating system technology for synthetic speech. I see you are using the operating system ") + ostype + bit + tr(". It should work fine. ") + *linux_str + tr(" I am licensed with General Public License version 3. You have full access to mine source code. I have been created using the application framework cute version ") + QT_VERSION_STR);
    mLibtexttospeech.setText(p);
}
void Dialog::dragEnterEvent(QDragEnterEvent *ev)
{
    ev->accept();
}
// publiv slots:
void Dialog::hello()
{
    QString *hello = new QString(QStringLiteral("Hello!"));
    mLibtexttospeech.setText(hello);
}

// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Qtexttospeech
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "dialog.h"
#include "ui_dialog.h"

void Dialog::checkForUpdates()
{
    QString *updateinstructions =
        new QString(tr("Please click on \"Update\""));
    auto *cfu = new CheckUpdate;
    cfu->check(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions);
    connect(cfu, &CheckUpdate::foundUpdate, [ = ](bool isupdated) {
        if(isupdated) {
        } else {
        }
    });
}
void Dialog::checkForUpdatesOnStart()
{
    QString *updateinstructions =
        new QString(tr("Please click on \"Update\""));
    auto *cfu = new CheckUpdate;
    cfu->checkOnStart(DISPLAY_NAME, VERSION, VERSION_PATH, *updateinstructions);
    connect(cfu, &CheckUpdate::foundUpdate, [ = ](bool isupdated) {
        if(isupdated) {
        } else {
        }
    });
}

// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Qtexttospeech
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
//    QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::PassThrough;
    ui->plainTextEdit->setFocus();
    // CONTEXT MENU
    this->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(this, &QWidget::customContextMenuRequested, [this]() {
        QSettings *settings = new QSettings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                                            EXECUTABLE_NAME);
        QMenu *contextMenu = new QMenu;
        QAction *actionSwedish = new QAction;
        actionSwedish->setIcon(QIcon(":/images/swedish.png"));
        actionSwedish->setText(tr("Swedish"));
        contextMenu->addAction(actionSwedish);
        QAction *actionEnglish = new QAction;
        actionEnglish->setIcon(QIcon(":/images/english.png"));
        actionEnglish->setText(tr("English"));
        contextMenu->addAction(actionEnglish);
        QAction *actionCustom = new QAction;
        actionCustom->setText(tr("Select language file"));
        contextMenu->addAction(actionCustom);
        QAction *actionChechUpdate = new QAction;
        actionChechUpdate->setText(tr("Check for updates"));
        contextMenu->addSeparator();
        contextMenu->addAction(actionChechUpdate);
        QAction *actionChechUpdateAtStart = new QAction;
        actionChechUpdateAtStart ->setText(tr("Check for updates at startup"));
        actionChechUpdateAtStart->setCheckable(true);
        settings->beginGroup("Updates");
        bool checkonstart = settings->value("checkupdateatstart").toBool();
        settings->endGroup();

        if(checkonstart) {
            actionChechUpdateAtStart->setChecked(true);
        } else {
            actionChechUpdateAtStart->setChecked(false);
        }

        contextMenu->addAction(actionChechUpdateAtStart);
        QAction *actionUpdate = new QAction;
        actionUpdate->setIcon(QIcon(":/images/update.png"));
        actionUpdate->setText(tr("Update"));
        contextMenu->addAction(actionUpdate);
#ifdef OFFLINE_INSTALLER
        QAction *actionUninstall = new QAction;
        actionUninstall ->setText(tr("Uninstall"));
        contextMenu->addAction(actionUninstall);
#endif
        contextMenu->addSeparator();
        QAction *actionDesktopShortcut = new QAction;
        actionDesktopShortcut->setCheckable(true);
        actionDesktopShortcut ->setText(tr("Desktop Shortcut"));
        settings->beginGroup("Shortcuts");
        bool desktopshortcut = settings->value("desktopshortcut").toBool();
        settings->endGroup();

        if(desktopshortcut) {
            actionDesktopShortcut->setChecked(true);
        } else {
            actionDesktopShortcut->setChecked(false);
        }

        contextMenu->addAction(actionDesktopShortcut);
#ifndef OFFLINE_INSTALLER
        QAction *actionApplicationShortcut = new QAction;
        actionApplicationShortcut->setCheckable(true);
        actionApplicationShortcut ->setText(tr("Applications menu Shortcut"));
        settings->beginGroup("Shortcuts");
        bool applicationmenushortcut = settings->value("applicationmenushortcut").toBool();
        settings->endGroup();

        if(applicationmenushortcut) {
            actionApplicationShortcut->setChecked(true);
        } else {
            actionApplicationShortcut->setChecked(false);
        }

        contextMenu->addAction(actionApplicationShortcut);
#endif
        // SWEDISH
        QObject::connect(actionSwedish, &QAction::triggered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Language");
            QString language = settings.value("language").toString();

            if(language == "sv_SE") {
                settings.endGroup();
                return;
            }

            settings.setValue("language", "sv_SE");
            settings.endGroup();
            settings.sync();
            const QString EXECUTE = QDir::toNativeSeparators(
                                        QCoreApplication::applicationDirPath() + "/" +
                                        QFileInfo(QCoreApplication::applicationFilePath()).fileName());
            QProcess p;
            p.setProgram(EXECUTE);
            p.startDetached();
            close();
        });
        // ENGLISH
        QObject::connect(actionEnglish, &QAction::triggered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Language");
            QString language = settings.value("language").toString();

            if(language == "no_translation") {
                settings.endGroup();
                return;
            }

            settings.setValue("language", "no_translation");
            settings.endGroup();
            settings.sync();
            const QString EXECUTE = (QCoreApplication::applicationDirPath() + "/" +
                                     QFileInfo(QCoreApplication::applicationFilePath()).fileName());
            QProcess p;
            p.setProgram(EXECUTE);
            p.startDetached();
            close();
        });
        // CUSTOM
        QObject::connect(actionCustom, &QAction::triggered, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Language");
            QString customlanguagepath = settings.value("customlanguagepath", QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toString();
            settings.endGroup();
            customlanguagepath = QFileDialog::getOpenFileName(this,
                                 tr("Open Language file"), customlanguagepath, tr("Language Files (*.qm)"));

            if(!customlanguagepath.isEmpty()) {
                settings.beginGroup("Language");
                settings.setValue("language", "custom");
                settings.setValue("customlanguagepath", customlanguagepath);
                settings.endGroup();
                settings.sync();
                const QString EXECUTE =
                    QCoreApplication::applicationDirPath() + "/" +
                    QFileInfo(QCoreApplication::applicationFilePath()).fileName();
                QProcess p;
                p.setProgram(EXECUTE);
                p.startDetached();
                close();
            }
        });
        // CHECK FOR UPDATES
        QObject::connect(actionChechUpdate, &QAction::triggered, [this]() {
            checkForUpdates();
        });
        // CHECK FOR UPDATES AT START
        QObject::connect(actionChechUpdateAtStart, &QAction::triggered, [actionChechUpdateAtStart]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Updates");

            if(actionChechUpdateAtStart->isChecked()) {
                settings.setValue("checkupdateatstart", true);
            } else {
                settings.setValue("checkupdateatstart", false);
            }

            settings.endGroup();
        });
        // UPDATE
        QObject::connect(actionUpdate, &QAction::triggered, [this]() {
#ifdef Q_OS_WINDOWS
#ifdef OFFLINE_INSTALLER
            close();
            DownloadInstall *mDownloadInstall;
            mDownloadInstall = new DownloadInstall(nullptr);
            QString *path = new QString(QStringLiteral(PATH));
            QString *filename = new QString(QStringLiteral(FILENAME));
            QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
            QString *version = new QString(QStringLiteral(VERSION));
            mDownloadInstall->setValues(path, filename, display_name, version);
            mDownloadInstall->show();
#endif
#ifndef OFFLINE_INSTALLER
            QDesktopServices::openUrl(QUrl(WINDOWS_DOWNLOADS, QUrl::TolerantMode));
#endif
#endif
#ifdef Q_OS_LINUX
            close();
            Update *up = new Update;
            // QDialog class
            ud = new UpdateDialog;
            ud->show();
            up->doUpdate(ARG1, ARG2, DISPLAY_NAME, ud);
#endif
        });
        // OFFLINE_INSTALLER
#ifdef OFFLINE_INSTALLER
        QObject::connect(actionUninstall, &QAction::triggered, [this]() {
            const QString EXECUTE = QCoreApplication::applicationDirPath() + "/uninstall.exe";
            QProcess p;
            p.setProgram(EXECUTE);
            p.startDetached();
            close();
        });
#endif
        // DESKTOP SHORTCUT
        QObject::connect(actionDesktopShortcut, &QAction::triggered, [actionDesktopShortcut]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Shortcuts");

            if(actionDesktopShortcut->isChecked()) {
                settings.setValue("desktopshortcut", true);
#ifdef Q_OS_WINDOWS
                Createshortcut *mCreateshortcut = new Createshortcut;
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
                mCreateshortcut->makeShortcutFile(display_name, executable_name, false, true);
#endif
#ifdef Q_OS_LINUX
                Createshortcut *mCreateshortcut = new Createshortcut;
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *comments = new QString(QStringLiteral("Text to speech."));
                QString *categories = new QString(QStringLiteral("AudioVideo"));
                QString *icon  = new QString(QStringLiteral("Qtexttospeech.png"));
                mCreateshortcut->makeShortcutFile(display_name, executable_name, comments, categories, icon, false, true);
#endif
            } else {
                settings.setValue("desktopshortcut", false);
#ifdef Q_OS_WINDOWS
                Createshortcut *mCreateshortcut = new Createshortcut;
                QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
                mCreateshortcut->removeDesktopShortcut(executable_name);
#endif
#ifdef Q_OS_LINUX
                Createshortcut *mCreateshortcut = new Createshortcut;
                QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
                mCreateshortcut->removeDesktopShortcut(executable_name);
#endif
            }

            settings.endGroup();
        });
#ifndef OFFLINE_INSTALLER
        // APPLICATIONS MENU SHORTCUT
        QObject::connect(actionApplicationShortcut, &QAction::triggered, [actionApplicationShortcut]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Shortcuts");

            if(actionApplicationShortcut->isChecked()) {
                settings.setValue("applicationmenushortcut", true);
#ifdef Q_OS_WINDOWS
                Createshortcut *mCreateshortcut = new Createshortcut;
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
                mCreateshortcut->makeShortcutFile(display_name, executable_name, true, false);
#endif
#ifdef Q_OS_LINUX
                Createshortcut *mCreateshortcut = new Createshortcut;
                QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
                QString *comments = new QString(QStringLiteral("Text to speech."));
                QString *categories = new QString(QStringLiteral("AudioVideo"));
                QString *icon  = new QString(QStringLiteral("Qtexttospeech.png"));
                mCreateshortcut->makeShortcutFile(display_name, executable_name, comments, categories, icon, true, false);
#endif
            } else {
                settings.setValue("applicationmenushortcut", false);
#ifdef Q_OS_WINDOWS
                Createshortcut *mCreateshortcut = new Createshortcut;
                QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
                mCreateshortcut->removeApplicationShortcut(executable_name);
#endif
#ifdef Q_OS_LINUX
                Createshortcut *mCreateshortcut = new Createshortcut;
                QString *executable_name = new QString(QStringLiteral(DISPLAY_NAME));
                mCreateshortcut->removeApplicationShortcut(executable_name);
#endif
            }

            settings.endGroup();
        });
#endif
        // CREATE SHORTCUT ON DESKTOP
        contextMenu->exec(QCursor::pos());
        contextMenu->clear();
        contextMenu->deleteLater();
    });
    //
    this->setWindowTitle("Qtexttospeech " VERSION);
    this->setWindowIcon(QIcon(":/images/texttospeech.png"));
    setStartConfig();
    QTimer::singleShot(1000, this, SLOT(available()));
    QTimer::singleShot(2000, this, SLOT(hello()));
#ifndef Q_OS_LINUX
    ui->cheUbuntu->setDisabled(true);
    ui->cheUbuntu->setVisible(false);
#endif
#ifdef Q_OS_LINUX
    ui->cheUbuntu->setEnabled(true);
    ui->cheUbuntu->setVisible(true);
#endif
    this->setAcceptDrops(true);
//    QPushButton *pb = new QPushButton(ui->pbResume);
//    pb->click();
    // EXIT God Bye
    connect(ui->pbExit, &QPushButton::pressed, [this]() {
        QString *text = new QString(tr("Goodbye!"));
        mLibtexttospeech.setText(text);
    });
    connect(ui->pbExit, &QPushButton::released, &mLibtexttospeech, &Libtexttospeech::finish);
    connect(&mLibtexttospeech, &Libtexttospeech::stateHasChenged, this, &Dialog::getState);
    // SET VOICE
    connect(ui->comVoices, &QComboBox::textActivated, [this]() {
        int i = ui->comVoices->currentIndex();
        mLibtexttospeech.setVoice(avaiableVoices.at(i));
        QString name = avaiableVoices.at(i).name();
        QString *_name = new QString(tr("My name is ") + name);
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Settings");
        settings.setValue("savedvoice", name);
        settings.endGroup();
        mLibtexttospeech.setText(_name);
    });
#ifdef Q_OS_LINUX
    // Workaround for Ubuntu
    connect(ui->cheUbuntu, &QCheckBox::stateChanged, [this]() {
        if(ui->cheUbuntu->isChecked()) {
            ui->sliVolume->setValue(10);
            ui->sliVolume->setDisabled(true);
        }

        if(!ui->cheUbuntu->isChecked()) {
            ui->sliVolume->setValue(10);
            ui->sliVolume->setEnabled(true);
        }
    });
#endif
    // SPEECH
    connect(ui->pbSpeak, &QPushButton::clicked, [this]() {
        QString in = ui->plainTextEdit->toPlainText();
        QString *text = new QString(in);
        mLibtexttospeech.setText(text);
    });
    // SET RATE
    connect(ui->sliRate, &QSlider::valueChanged, [this]() {
        int in = ui->sliRate->value();
        mLibtexttospeech.setRate(in);
        ui->lblSetRate->setText(tr("Rate: ") + QString::number(ui->sliRate->value()));
    });
    // SET PITCH
    connect(ui->sliPitc, &QSlider::valueChanged, [this]() {
        int in = ui->sliPitc->value();
        mLibtexttospeech.setPitch(in);
        ui->lblSetPitch->setText(tr("Pitch: ") + QString::number(ui->sliPitc->value()));
    });
    // SET VOLUME
    connect(ui->sliVolume, &QSlider::valueChanged, [this]() {
        int in = ui->sliVolume->value();
        mLibtexttospeech.setVolume(in);
        ui->lblSetVolume->setText(tr("Volume: ") + QString::number(ui->sliVolume->value()));
    });
    // LOCALE
    connect(ui->comLocale, &QComboBox::textActivated, [this]() {
        int i = ui->comLocale->currentIndex();
        mLibtexttospeech.setLocale(avaiableLocal.at(i));
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Settings");
        settings.setValue("savedlocale", avaiableLocal.at(i).name());
        settings.endGroup();
    });
    // EXIT
    connect(ui->pbExit, &QPushButton::clicked, [this]() {
        close();
    });
    // PAUSE
    connect(ui->pbPause, &QPushButton::clicked, [this]() {
        mLibtexttospeech.setPause();
    });
    // STOP
    connect(ui->pbStop, &QPushButton::clicked, [this]() {
        mLibtexttospeech.setStop();
    });
    // CLEAR
    connect(ui->pbClear, &QPushButton::clicked, [this]() {
        ui->plainTextEdit->clear();
        ui->plainTextEdit->setFocus();
    });
    // RESUME
    connect(ui->pbResume, &QPushButton::clicked, [this]() {
        mLibtexttospeech.setResume();
    });
    // ABOUT
    connect(ui->pbAbout, &QPushButton::clicked, this, &Dialog::about);
}
Dialog::~Dialog()
{
    delete ui;
}
//public slots:
void Dialog::getState(QString * state)
{
    ui->lblStateChanged->setText(tr("State: ") + *state);
}

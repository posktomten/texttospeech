// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Qtexttospeech
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>




#ifndef DIALOG_H
#define DIALOG_H
#include "checkupdate.h"
#include "createshortcut.h"
#include "texttospeech.h"
#include <QContextMenuEvent>
#include <QDebug>
#include <QDesktopServices>
#include <QDialog>
#include <QDir>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QEvent>
#include <QFileDialog>
#include <QFontDatabase>
#include <QMenu>
#include <QMimeData>
#include <QNetworkAccessManager>
#include <QOperatingSystemVersion>
#include <QProcess>
#include <QSettings>
#include <QStandardPaths>
#include <QTimer>
#include <QTranslator>
#include <QWheelEvent>

#ifdef Q_OS_WINDOWS
#include "download_install.h"
#endif
#ifdef Q_OS_LINUX
#define USER_MESSAGE "http://bin.ceicer.com/texttospeech/user_linux"
#define VERSION_PATH "http://bin.ceicer.com/texttospeech/version_linux"
#include "update.h"
#include "updatedialog.h"
//EDIT
#define HIGH_GLIBC
#define ZSYNC2
#endif
#ifdef Q_OS_WINDOWS
#define WINDOWS_DOWNLOADS "https://gitlab.com/posktomten/texttospeech/-/wikis/DOWNLOADS/Windows"
#define USER_MESSAGE "http://bin.ceicer.com/texttospeech/user_windows"
#define VERSION_PATH "http://bin.ceicer.com/texttospeech/version_windows"
//EDIT
#define OFFLINE_INSTALLER
#endif

#define VERSION "1.1.1"
#define DISPLAY_NAME "Qtexttospeech"
#define EXECUTABLE_NAME "Qtexttospeech"


#ifdef Q_OS_WINDOWS
#ifdef Q_PROCESSOR_X86_64 // 64 bit
#define PATH "http://bin.ceicer.com/texttospeech/bin/"
#define FILENAME "Qtexttospeech_64-bit_setup.exe"
#endif
#ifdef Q_PROCESSOR_X86_32 // 64 bit
#define PATH "http://bin.ceicer.com/texttospeech/bin/"
#define FILENAME "Qtexttospeech_32-bit_setup.exe"
#endif
#endif
#ifdef Q_OS_LINUX
#define FONTSIZE 13
#endif
#ifdef Q_OS_WINDOWS
#define FONTSIZE 11
#endif

//
#ifdef Q_OS_LINUX
#include "update.h"
#include "updatedialog.h"
#ifdef Q_PROCESSOR_X86_64 // 64 bit
#ifndef HIGH_GLIBC
#define ARG1 "Qtexttospeech-x86_64.AppImage"
#define ARG2 "https://bin.ceicer.com/zsync/Qtexttospeech-x86_64.AppImage.zsync"
#endif
#ifdef HIGH_GLIBC
#ifndef ZSYNC2
#define ARG1 "Qtexttospeech-x86_64.AppImage"
#define ARG2 "https://bin.ceicer.com/zsync/HIGH_GLIBC/Qtexttospeech-x86_64.AppImage.zsync"
#endif
#ifdef ZSYNC2
#define ARG1 "Qtexttospeech-x86_64.AppImage"
#define ARG2 "https://bin.ceicer.com/zsync2/HIGH_GLIBC/Qtexttospeech-x86_64.AppImage.zsync"
#endif
#endif
#endif
#ifdef Q_PROCESSOR_X86_32 // 32 bit
#define ARG1 "Qtexttospeech-i386.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/Qtexttospeech-i386.AppImage.zsync"
#endif
#endif
//



QT_BEGIN_NAMESPACE
namespace Ui
{
class Dialog;
}
QT_END_NAMESPACE

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *parent = nullptr);
    ~Dialog();
    void setEndConfig();

protected:
    void dropEvent(QDropEvent *ev);
    void dragEnterEvent(QDragEnterEvent *ev);
    void wheelEvent(QWheelEvent *event);


private:
    Ui::Dialog *ui;
    Libtexttospeech mLibtexttospeech;
//    void update();
    void setStartConfig();
    void checkForUpdates();
    void checkForUpdatesOnStart();
#ifdef Q_OS_LINUX // Linux
    UpdateDialog *ud;
#endif

    //
    QVector<QVoice> avaiableVoices;
    QVector<QLocale> avaiableLocal;
//    QVector<QLocale> avaiableEngines;
    QStringList avaiableEngines;
    void userMessage();


public slots:
    void getState(QString *state);
    void available();
    void hello();
    void about();

};
#endif // DIALOG_H

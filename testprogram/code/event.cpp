// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Qtexttospeech
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "dialog.h"
#include "ui_dialog.h"



void Dialog::wheelEvent(QWheelEvent * event)
{
    if(ui->plainTextEdit->underMouse()) {
        if(ui->plainTextEdit->font().pointSize() > 60)  {
            ui->plainTextEdit->zoomOut();
            return;
        } else if(ui->plainTextEdit->font().pointSize() < 7)  {
            ui->plainTextEdit->zoomIn();
            return;
        }

        if(event->angleDelta().y() > 0) {
            ui->plainTextEdit->zoomIn();
        } else if(event->angleDelta().y() < 0) {
            ui->plainTextEdit->zoomOut();
        }

        event->accept();
    }

    event->ignore();
}


<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="55"/>
        <source>Engines</source>
        <translation>Motorer</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="90"/>
        <source>Locales</source>
        <translation>Platser</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="125"/>
        <source>Voices</source>
        <translation>Röster</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="158"/>
        <source>Solution for Ubuntu. Possibly also for other Linux distributions.</source>
        <translation>Lösning för Ubuntu. Möjligen också för andra Linux-distributioner.</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="171"/>
        <source>Speak at drag and drop.</source>
        <translation>Tala vid drag och släpp.</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="206"/>
        <source>Volume</source>
        <translation>Volym</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="250"/>
        <source>Pitch</source>
        <translation>Tonhöjd</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="291"/>
        <source>Rate</source>
        <translation>Takt</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="348"/>
        <source>Speak!</source>
        <translation>Tala!</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="373"/>
        <source>Pause</source>
        <translation>Paus</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="398"/>
        <source>Resume</source>
        <translation>Återuppta</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="423"/>
        <source>Stop</source>
        <translation>Stopp</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="448"/>
        <source>Clear</source>
        <translation>Rensa</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="473"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="498"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="529"/>
        <source>State:</source>
        <translation>Tillstånd:</translation>
    </message>
    <message>
        <source>Paste text here or drag and drop a text file.</source>
        <translation type="vanished">Klistra in text här eller dra och släpp en textfil.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="38"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="42"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="45"/>
        <source>Select language file</source>
        <translation>Välj språkfil</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="48"/>
        <source>Check for updates</source>
        <translation>Sök efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="52"/>
        <source>Check for updates at startup</source>
        <translation>Sök efter uppdateringar vid programstart</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="67"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="71"/>
        <source>Uninstall</source>
        <translation>Avinstallera</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="77"/>
        <source>Desktop Shortcut</source>
        <translation>Genväg på skrivbordet</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="92"/>
        <source>Applications menu Shortcut</source>
        <translation>Genväg i programmenyn</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="158"/>
        <source>Open Language file</source>
        <translation>Öppna språkfil</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="158"/>
        <source>Language Files (*.qm)</source>
        <translation>Språkfiler (*.qm)</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="334"/>
        <source>Goodbye!</source>
        <translation>Adjö!</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="344"/>
        <source>My name is </source>
        <translation>Mitt namn är </translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="376"/>
        <location filename="../setstartconfig.cpp" line="40"/>
        <source>Rate: </source>
        <translation>Takt: </translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="382"/>
        <location filename="../setstartconfig.cpp" line="43"/>
        <source>Pitch: </source>
        <translation>Tonhöjd: </translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="431"/>
        <source>State: </source>
        <translation>Tillstånd: </translation>
    </message>
    <message>
        <location filename="../available.cpp" line="114"/>
        <source>. You are using a sixty-four bit operating system</source>
        <translation>. Du använder ett sextiofyrabitars operativsystem</translation>
    </message>
    <message>
        <location filename="../available.cpp" line="116"/>
        <source>. You are using a thirty-two bit operating system</source>
        <translation>. Du använder ett tretiotvåbitars operativsystem</translation>
    </message>
    <message>
        <location filename="../available.cpp" line="120"/>
        <source>Except that this program may crash if you touch the volume control.</source>
        <translation>Förutom att det här programmet kan krascha om du ändrar på volymkontrollen.</translation>
    </message>
    <message>
        <location filename="../available.cpp" line="125"/>
        <source>. It should work fine. </source>
        <translation>. det borde fungera fint. </translation>
    </message>
    <message>
        <location filename="../available.cpp" line="125"/>
        <source> I am licensed with General Public License version 3. You have full access to mine source code. I have been created using the application framework cute version </source>
        <translation> Jag är licensierad med General Public License version 3. Du har full tillgång till min källkod. Jag har skapats med hälp av applikationsramverket qt version </translation>
    </message>
    <message>
        <source> I am licensed with General Public License version 3. You have full access to mine source code.</source>
        <translation type="vanished"> Jag är licensierad med General Public License version 3. Du har full tillgång till min källkod.</translation>
    </message>
    <message>
        <location filename="../available.cpp" line="125"/>
        <source>I&apos;m going to do my very best. Please write or paste the text you want me to read aloud. You can also drag and drop a text file to me. Then I will read it to you. I use the operating system technology for synthetic speech. I see you are using the operating system </source>
        <translation>Jag ska göra mitt allra bästa. Skriv eller klistra in texten du vill att jag ska läsa upp. Du kan också dra och släppa en textfil till mig. Då ska jag läsa den för dig. Jag använder operativsystemets tekniken för syntetiskt tal. Jag ser att du använder operativsystemet </translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="388"/>
        <location filename="../setstartconfig.cpp" line="32"/>
        <source>Volume: </source>
        <translation>Volym: </translation>
    </message>
    <message>
        <location filename="../check_for_updates.cpp" line="25"/>
        <location filename="../check_for_updates.cpp" line="37"/>
        <source>Please click on &quot;Update&quot;</source>
        <translation>Vänligen klicka på &quot;Uppdatera&quot;</translation>
    </message>
    <message>
        <location filename="../setstartconfig.cpp" line="79"/>
        <source>Paste text here or drag and drop a text file.
Right-click on the program to access more options.</source>
        <translation>Klistra in text här eller dra och släpp en textfil.
Högerklicka på programmet för att komma åt fler alternativ.</translation>
    </message>
</context>
</TS>

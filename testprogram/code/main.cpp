// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Qtexttospeech
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "dialog.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication *a = new QApplication(argc, argv);
    int id = QFontDatabase::addApplicationFont(":/fonts/Ubuntu-R.ttf");
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont *font = new QFont(family);
    font->setFamily(family);
    font->setPointSize(FONTSIZE);
    a->setFont(*font);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Language");
    QString language = settings.value("language", "en_US").toString();
    settings.endGroup();
#ifdef Q_OS_LINUX
    const QString qttranslationsPath(":/i18n/linux/");
#endif
#ifdef Q_OS_WINDOWS
    const QString qttranslationsPath(":/i18n/windows/");
#endif
    QTranslator *qtTranslator = new QTranslator;

    if(language == "custom") {
        settings.beginGroup("Language");
        QString customlanguage = settings.value("customlanguage").toString();
        settings.endGroup();

        if(qtTranslator->load(customlanguage)) {
            QApplication::installTranslator(qtTranslator);
        }
    } else if(language != "no_translation") {
        if(qtTranslator->load(qttranslationsPath + "texttosspech_" + language + ".qm")) {
            QApplication::installTranslator(qtTranslator);
        }
    }

    Dialog *w = new Dialog;
    // endConfig
    w->setFont(*font);
    QObject::connect(a, &QCoreApplication::aboutToQuit,
                     [&]() -> void { w->setEndConfig(); });
    w->show();
    int slut = a->exec();
    delete qtTranslator;
    return slut;
}

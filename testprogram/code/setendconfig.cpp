// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Qtexttospeech
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "dialog.h"
#include "ui_dialog.h"

void Dialog::setEndConfig()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);;
    settings.beginGroup("Settings");
#ifdef Q_OS_WINDOWS
    int volume_value = ui->sliVolume->value();
    settings.setValue("volume", volume_value);
#endif
#ifdef Q_OS_LINUX
    bool checked = ui->cheUbuntu->isChecked();
    settings.setValue("cheubuntu", checked);
#endif
    int value = ui->sliRate->value();
    settings.setValue("rate", value);
    value = ui->sliPitc->value();
    settings.setValue("pitch", value);
    bool ischecked = ui->cheSpeak->isChecked();
    settings.setValue("chespeak", ischecked);
    QFont f = ui->plainTextEdit->font();
    int fontsize = f.pointSize();
    settings.setValue("fontsize", fontsize);
    settings.endGroup();
    settings.beginGroup("Geometry");
    settings.setValue("savegeometry", this->saveGeometry());
    settings.endGroup();
    QString content = ui->plainTextEdit->toPlainText();
    QFileInfo fi(settings.fileName());
    QString path = fi.absolutePath();
    QFile file(path + "/savedcontent");

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);
    out << content;
}

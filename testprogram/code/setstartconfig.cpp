// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Qtexttospeech
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "dialog.h"
#include "ui_dialog.h"

void Dialog::setStartConfig()
{
#ifdef OFFLINE_INSTALLER
    const QString tmppath = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    QFile fil(tmppath + "/" FILENAME);

    if(fil.exists()) {
        fil.remove();
    }

#endif
    userMessage();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Settings");
    int volume = settings.value("volume").toInt();
#ifdef Q_OS_WINDOWS
    ui->sliVolume->setValue(volume);
#endif
    ui->lblSetVolume->setText(tr("Volume: ") + QString::number(volume));
#ifdef Q_OS_LINUX
    bool checked = settings.value("cheubuntu").toBool();
    ui->cheUbuntu->setChecked(checked);
    ui->sliVolume->setDisabled(checked);
#endif
    int value = settings.value("rate").toInt();
    ui->sliRate->setValue(value);
    ui->lblSetRate->setText(tr("Rate: ") + QString::number(value));
    value = settings.value("pitch").toInt();
    ui->sliPitc->setValue(value);
    ui->lblSetPitch->setText(tr("Pitch: ") + QString::number(value));
    bool ischecked = settings.value("chespeak").toBool();
    ui->cheSpeak->setChecked(ischecked);
    int fontsize = settings.value("fontsize", 11).toInt();
    settings.endGroup();
    settings.beginGroup("Updates");

    if(settings.value("checkupdateatstart").toBool()) {
        checkForUpdatesOnStart();
    }

    settings.endGroup();
    int id = QFontDatabase::addApplicationFont(":/fonts/Ubuntu-R.ttf");
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont *font = new QFont(family);
    font->setFamily(family);
    font->setPointSize(fontsize);
    ui->plainTextEdit->setFont(*font);
    settings.beginGroup("Geometry");
    this->restoreGeometry(settings.value("savegeometry").toByteArray());
    settings.endGroup();
    QFileInfo fi(settings.fileName());
    QString path = fi.absolutePath();
    QFile file(path + "/savedcontent");

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file);
    QString content;

    while(!in.atEnd()) {
        content = in.readLine();
        ui->plainTextEdit->appendPlainText(content);
    }

    ui->plainTextEdit->setPlaceholderText(tr("Paste text here or drag and drop a text file.\nRight-click on the program to access more options."));
}

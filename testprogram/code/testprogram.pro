
#// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          Qtexttospeech
#//          Copyright (C) 2022 Ingemar Ceicer
#//          https://gitlab.com/posktomten/texttospeech
#//          programmering1@ceicer.com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License as published by
#//   the Free Software Foundation version 3.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


QT       += core gui widgets texttospeech network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    available.cpp \
    check_for_updates.cpp \
    event.cpp \
    main.cpp \
    dialog.cpp \
    setendconfig.cpp \
    setstartconfig.cpp \
    user_message.cpp

HEADERS += \
    dialog.h
    TRANSLATIONS += \
        i18n/_texttosspech_testprg_sv_SE.ts \
        i18n/_texttosspech_testprg_template_xx_XX.ts

FORMS += \
    dialog.ui

unix:INCLUDEPATH += "../include_linux"
unix:DEPENDPATH += "../include_linux"
win32:INCLUDEPATH += "../include_windows"
win32:DEPENDPATH += "../include_windows"

TARGET = Qtexttospeech

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

equals(QT_MAJOR_VERSION, 5) {
DESTDIR="../build-executable5"

CONFIG (release, debug|release): LIBS += -L../lib5/ -ltexttospeech  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -ltexttospeechd # Debug

CONFIG (release, debug|release): LIBS += -L../lib5/ -lcheckupdate # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcheckupdated # Debug

CONFIG (release, debug|release): LIBS += -L../lib5/ -lcreateshortcut  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcreateshortcutd # Debug

win32: CONFIG (release, debug|release): LIBS += -L../lib5/ -ldownload_install # Release
else: win32: CONFIG (debug, debug|release): LIBS += -L../lib5/ -ldownload_installd # Debug

unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lupdateappimaged # Debug

}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR="../build-executable6"

CONFIG (release, debug|release): LIBS += -L../lib6/ -ltexttospeech  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -ltexttospeechd # Debug

CONFIG (release, debug|release): LIBS += -L../lib6/ -lcheckupdate # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lcheckupdated # Debugg

CONFIG (release, debug|release): LIBS += -L../lib6/ -lcreateshortcut  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lcreateshortcutd # Debug

win32: CONFIG (release, debug|release): LIBS += -L../lib6/ -ldownload_install # Release
else: win32: CONFIG (debug, debug|release): LIBS += -L../lib6/ -ldownload_installd # Debug

unix: CONFIG  (release, debug|release): LIBS += -L../lib6/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib6/ -lupdateappimaged # Debug

}

RESOURCES += \
    resources.qrc

RC_FILE = myapp.rc

message (----------------------------------------)
message (OS: $$QMAKE_HOST.os)
#message (Arch: $$QMAKE_HOST.arch)
#message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (QMAKE_QMAKE: $$QMAKE_QMAKE)
#message (QT_VERSION: $$QT_VERSION)
message(_PRO_FILE_PWD_: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)


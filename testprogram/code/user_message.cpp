// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Qtexttospeech
//          Copyright (C) 2022 Ingemar Ceicer
//          https://gitlab.com/posktomten/texttospeech
//          programmering1@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation version 3.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "dialog.h"
#include "ui_dialog.h"

void Dialog::userMessage()
{
    QUrl url(USER_MESSAGE);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
//    reply->waitForBytesWritten(1000);

    if(reply->error() != QNetworkReply::NoError) {
        return;
    }

    QObject::connect(
        reply, &QNetworkReply::finished,
    [reply, this] {
        QByteArray *bytes = new QByteArray;
        *bytes = reply->readAll();


        if(reply->error() == QNetworkReply::ContentNotFoundError)
        {
            return;
        }
        QString s(*bytes);
        s = s.trimmed();


        if(s.isEmpty())
        {
            return;
        }
        ui->plainTextEdit->insertPlainText(s);
//        QTextCursor cursor = ui->teOut->textCursor();
        ui->plainTextEdit->moveCursor(QTextCursor::Start);

    });
}

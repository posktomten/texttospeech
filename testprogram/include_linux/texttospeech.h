#ifndef LIBTEXTTOSPEECH_H
#define LIBTEXTTOSPEECH_H

#include "texttospeech_global.h"
#include <QTextToSpeech>
#include <QLocale>
#include <QObject>
#include <QDebug>
#include <QVoice>
#include <QtCore/qglobal.h>
#ifdef Q_OS_WINDOWS
#ifndef QT_STATIC
class LIBTEXTTOSPEECH_EXPORT Libtexttospeech : public QObject
#endif

#ifdef QT_STATIC
    class Libtexttospeech : public QObject
#endif
#endif
#ifdef Q_OS_LINUX
        class Libtexttospeech : public QObject
#endif
        {
            Q_OBJECT
        public:

            Libtexttospeech();
            ~Libtexttospeech();
            void setText(const QString *text);
            void setPitch(const int inputPitch);
            void setRate(const int inputRate);
            void setVolume(const int inputVolume);
            void setStop();
            void setPause();
            void setResume();
            void setLocale(QLocale locale);

            void setVoice(QVoice voice);
//            void setEngine(QTextToSpeech endgine);
            QVector<QLocale> getLocales();
            QVector<QVoice> getVoices();



        private slots:
            void getStateHasChenged(QTextToSpeech::State state);
        public slots:
            void finish();


        signals:
            void stateHasChenged(QString *state);

        private:
            QTextToSpeech *speech;

        };

#endif // LIBTEXTTOSPEECH_H

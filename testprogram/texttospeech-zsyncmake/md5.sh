#!/bin/bash
date_now=$(date "+%FT%H-%M-%S")
READMORE="https://gitlab.com/posktomten/texttospeech"
for i in *; do # Whitespace-safe but not recursive.
	echo "--------------------------------------------------" > "$i-MD5.txt"
	echo "MD5 HASH SUM" >> "$i-MD5.txt"
	md5sum "$i" >> "$i-MD5.txt"
	filesize=$(stat -c%s "$i")
	mb=$(bc <<<"scale=6; $filesize / 1048576")
	echo "Created $date_now" >> "$i-MD5.txt"
	echo "Size $mb MB." >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "HOW TO CHECK THE HASH SUM" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "LINUX" >> "$i-MD5.txt"
	echo "\"md5sum\" is included in most Linus distributions." >> "$i-MD5.txt"
	echo "md5sum $i" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "WINDOWS" >> "$i-MD5.txt"
	echo "\"certutil\" is included in Windows." >> "$i-MD5.txt"
	echo "certutil -hashfile $i md5" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
	echo "READ MORE" >> "$i-MD5.txt"
	echo "${READMORE}" >> "$i-MD5.txt"
	echo "--------------------------------------------------" >> "$i-MD5.txt"
done
rm md5.sh-MD5.txt

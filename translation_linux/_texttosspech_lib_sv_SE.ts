<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Libtexttospeech</name>
    <message>
        <location filename="../texttospeech.cpp" line="70"/>
        <source>Ready</source>
        <translation>Berädd</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="72"/>
        <source>Speaking</source>
        <translation>Talar</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="74"/>
        <source>Paused</source>
        <translation>Pausad</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="76"/>
        <source>BackendError (No working speech synthesis in the operating system)</source>
        <translation>BackendError (Ingen fungerande talsyntes i operativsystemet)</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="78"/>
        <source>Unknown error</source>
        <translation>Okänt fel</translation>
    </message>
</context>
</TS>

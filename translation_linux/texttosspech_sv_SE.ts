<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>CheckUpdate</name>
    <message>
        <location filename="../checkupdate.cpp" line="53"/>
        <location filename="../checkupdate.cpp" line="74"/>
        <location filename="../checkupdate.cpp" line="145"/>
        <location filename="../checkupdate.cpp" line="259"/>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation>Ingen internetanslutning hittades.
Kontrollera dina Internetinställningar och brandvägg.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="91"/>
        <source>Your version of </source>
        <translation>Din version av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="92"/>
        <source> is newer than the latest official version. </source>
        <translation> är nyare än den senaste officiella versionen. </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="101"/>
        <source>You have the latest version of </source>
        <translation>Du har senaste versonen av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="110"/>
        <source>There was an error when the version was checked.</source>
        <translation>Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="177"/>
        <source>
There was an error when the version was checked.</source>
        <translation>
Det inträffade ett fel när versionen kontrollerades.</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="222"/>
        <source>Updates:</source>
        <translation>Uppdateringar:</translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="245"/>
        <source>There is a new version of </source>
        <translation>Det finns en ny version av </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="247"/>
        <source>Latest version: </source>
        <translation>Senaste versionen: </translation>
    </message>
    <message>
        <location filename="../checkupdate.cpp" line="35"/>
        <location filename="../checkupdate.cpp" line="51"/>
        <location filename="../checkupdate.cpp" line="75"/>
        <location filename="../checkupdate.cpp" line="93"/>
        <location filename="../checkupdate.cpp" line="102"/>
        <location filename="../checkupdate.cpp" line="111"/>
        <location filename="../checkupdate.cpp" line="143"/>
        <location filename="../checkupdate.cpp" line="178"/>
        <location filename="../checkupdate.cpp" line="251"/>
        <location filename="../checkupdate.cpp" line="260"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="29"/>
        <source>No error.</source>
        <translation>Inga fel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="33"/>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Fjärrservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="37"/>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation>Fjärrvärdnamnet hittades inte (ogiltigt värdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="41"/>
        <source>The connection to the remote server timed out.</source>
        <translation>Anslutningen till fjärrservern tog för lång tid.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="45"/>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation>Operationen avbröts via anrop till abort() eller close() innan den avslutades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="49"/>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation>SSL/TLS-handskakningen misslyckades och den krypterade kanalen kunde inte upprättas. Signalen sslErrors() borde ha sänts ut.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="53"/>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket, men systemet har initierat roaming till en annan åtkomstpunkt. Begäran bör skickas in på nytt och kommer att behandlas så snart anslutningen återupprättats.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="57"/>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation>Anslutningen bröts på grund av avbrott från nätverket eller misslyckande med att starta nätverket.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="61"/>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation>Bakgrundsbegäran är för närvarande inte tillåten på grund av plattformspolicy.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="65"/>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation>Omdirigeringarna nådde maxgränsen. Gränsen är som standard inställd på 50 eller enligt QNetworkRequest::setMaxRedirectsAllowed().</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="69"/>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation>Vid omdirigeringar upptäckte API:et för nätverksåtkomst en omdirigering från ett krypterat protokoll (https) till ett okrypterat (http).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="73"/>
        <source>An unknown network-related error was detected.</source>
        <translation>Ett okänt nätverksrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="77"/>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation>Anslutningen till proxyservern nekades (proxyservern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="81"/>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation>Proxyservern stängde anslutningen i förtid, innan hela svaret togs emot och bearbetades.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="85"/>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation>Proxyvärdnamnet hittades inte (ogiltigt proxyvärdnamn).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="89"/>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation>Anslutningen till proxyn tog för läng tid eller så svarade proxyn inte i tid på den skickade begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="93"/>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation>Proxyn kräver autentisering för att uppfylla begäran men accepterade inte erbjudna användarnamn och lösenord (eller så har inga användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="97"/>
        <source>An unknown proxy-related error was detected.</source>
        <translation>Ett okänt proxyrelaterat fel upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="101"/>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation>Åtkomsten till fjärrinnehållet nekades (liknande HTTP-fel 403).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="105"/>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation>Åtgärden som begärs på fjärrinnehållet är inte tillåten.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="109"/>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation>Fjärrinnehållet hittades inte på servern (liknande HTTP-fel 404).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="113"/>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation>Fjärrservern kräver autentisering för att visa innehållet men angivna användarnamn och lösenord accepterades inte (eller så har inget användarnamn och lösenord skickats).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="117"/>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation>Begäran behövde skickas igen, men detta misslyckades till exempel eftersom uppladdningsdata inte kunde läsas en andra gång.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="121"/>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation>Begäran kunde inte slutföras på grund av en konflikt med resursens nuvarande tillstånd.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="125"/>
        <source>The requested resource is no longer available at the server.</source>
        <translation>Den begärda resursen är inte längre tillgänglig på servern.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="129"/>
        <source>An unknown error related to the remote content was detected.</source>
        <translation>Ett okänt fel relaterat till fjärrinnehållet upptäcktes.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="133"/>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation>Network Access API kan inte uppfylla begäran eftersom protokollet inte är känt.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="137"/>
        <source>The requested operation is invalid for this protocol.</source>
        <translation>Den begärda åtgärden är ogiltig för detta protokoll.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="141"/>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation>Ett haveri i protokollet upptäcktes (analysfel, ogiltiga eller oväntade svar, etc.).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="145"/>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation>Servern stötte på ett oväntat tillstånd som hindrade den från att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="149"/>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation>Servern stöder inte den funktionalitet som krävs för att uppfylla begäran.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="153"/>
        <source>The server is unable to handle the request at this time.</source>
        <translation>Servern kan inte hantera begäran just nu.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="157"/>
        <source>Unknown Server Error.</source>
        <translation>Okänt serverfel.</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="161"/>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation>Fjärrservern vägrade anslutningen (servern accepterar inte förfrågningar).</translation>
    </message>
    <message>
        <location filename="../networkerrormessages.cpp" line="165"/>
        <source>Unknown Error.</source>
        <translation>Okänt fel.</translation>
    </message>
</context>
<context>
    <name>Createshortcut</name>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <source>not found.&lt;br&gt;The shortcut will be created without an icon.</source>
        <translation>hittas inte.&lt;br&gt;Genvägen kommer att skapas utan ikon.</translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="37"/>
        <location filename="../createshortcut.cpp" line="48"/>
        <location filename="../createshortcut.cpp" line="111"/>
        <location filename="../removeshortcut.cpp" line="38"/>
        <location filename="../removeshortcut.cpp" line="60"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="47"/>
        <source>not found.&lt;br&gt;The shortcut will not be created</source>
        <translation>hittas inte.&lt;br&gt;Genvägen kommer inte att skapas</translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="110"/>
        <source>The shortcut could not be created.&lt;br&gt;Check your file permissions.</source>
        <translation>Genvägen kunde inte skapas.&lt;br&gt;Kolla dina filrättigheter.</translation>
    </message>
    <message>
        <location filename="../removeshortcut.cpp" line="37"/>
        <location filename="../removeshortcut.cpp" line="59"/>
        <source> can not be removed.&lt;br&gt;Pleas check your file permissions.</source>
        <translation> kan inte tas bort.&lt;br&gt;Vänligen kontrollera dina filrättigheter.</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="../update.cpp" line="34"/>
        <location filename="../update.cpp" line="67"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="35"/>
        <source>zsync cannot be found in path:
</source>
        <translation>zsync kan inte hittas i sökvägen:
</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="36"/>
        <source>Unable to update.</source>
        <translation>Kan inte uppdatera.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="68"/>
        <source>An unexpected error occurred. Error message:</source>
        <translation>Ett oväntat fel uppstod. Felmeddelande:</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="78"/>
        <source>is updated.</source>
        <translation>är uppdaterad.</translation>
    </message>
    <message>
        <location filename="../update.cpp" line="79"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../updatedialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="31"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../updatedialog.cpp" line="47"/>
        <source>Updating, please wait...</source>
        <translation>Uppdaterar, vänta...</translation>
    </message>
</context>
<context>
    <name>Libtexttospeech</name>
    <message>
        <location filename="../texttospeech.cpp" line="70"/>
        <source>Ready</source>
        <translation>Berädd</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="72"/>
        <source>Speaking</source>
        <translation>Talar</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="74"/>
        <source>Paused</source>
        <translation>Pausad</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="76"/>
        <source>BackendError (No working speech synthesis in the operating system)</source>
        <translation>BackendError (Ingen fungerande talsyntes i operativsystemet)</translation>
    </message>
    <message>
        <location filename="../texttospeech.cpp" line="78"/>
        <source>Unknown error</source>
        <translation>Okänt fel</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="55"/>
        <source>Engines</source>
        <translation>Motorer</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="90"/>
        <source>Locales</source>
        <translation>Platser</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="125"/>
        <source>Voices</source>
        <translation>Röster</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="158"/>
        <source>Solution for Ubuntu. Possibly also for other Linux distributions.</source>
        <translation>Lösning för Ubuntu. Möjligen också för andra Linux-distributioner.</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="171"/>
        <source>Speak at drag and drop.</source>
        <translation>Tala vid drag och släpp.</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="206"/>
        <source>Volume</source>
        <translation>Volym</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="250"/>
        <source>Pitch</source>
        <translation>Tonhöjd</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="291"/>
        <source>Rate</source>
        <translation>Takt</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="348"/>
        <source>Speak!</source>
        <translation>Tala!</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="373"/>
        <source>Pause</source>
        <translation>Paus</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="398"/>
        <source>Resume</source>
        <translation>Återuppta</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="423"/>
        <source>Stop</source>
        <translation>Stopp</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="448"/>
        <source>Clear</source>
        <translation>Rensa</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="473"/>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="498"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="529"/>
        <source>State:</source>
        <translation>Tillstånd:</translation>
    </message>
    <message>
        <source>Paste text here or drag and drop a text file.</source>
        <translation type="vanished">Klistra in text här eller dra och släpp en textfil.</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="38"/>
        <source>Swedish</source>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="42"/>
        <source>English</source>
        <translation>Engelska</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="45"/>
        <source>Select language file</source>
        <translation>Välj språkfil</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="48"/>
        <source>Check for updates</source>
        <translation>Sök efter uppdateringar</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="52"/>
        <source>Check for updates at startup</source>
        <translation>Sök efter uppdateringar vid programstart</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="67"/>
        <source>Update</source>
        <translation>Uppdatera</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="71"/>
        <source>Uninstall</source>
        <translation>Avinstallera</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="77"/>
        <source>Desktop Shortcut</source>
        <translation>Genväg på skrivbordet</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="92"/>
        <source>Applications menu Shortcut</source>
        <translation>Genväg i programmenyn</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="158"/>
        <source>Open Language file</source>
        <translation>Öppna språkfil</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="158"/>
        <source>Language Files (*.qm)</source>
        <translation>Språkfiler (*.qm)</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="334"/>
        <source>Goodbye!</source>
        <translation>Adjö!</translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="344"/>
        <source>My name is </source>
        <translation>Mitt namn är </translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="376"/>
        <location filename="../setstartconfig.cpp" line="40"/>
        <source>Rate: </source>
        <translation>Takt: </translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="382"/>
        <location filename="../setstartconfig.cpp" line="43"/>
        <source>Pitch: </source>
        <translation>Tonhöjd: </translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="388"/>
        <location filename="../setstartconfig.cpp" line="32"/>
        <source>Volume: </source>
        <translation>Volym: </translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="431"/>
        <source>State: </source>
        <translation>Tillstånd: </translation>
    </message>
    <message>
        <location filename="../available.cpp" line="114"/>
        <source>. You are using a sixty-four bit operating system</source>
        <translation>. Du använder ett sextiofyrabitars operativsystem</translation>
    </message>
    <message>
        <location filename="../available.cpp" line="116"/>
        <source>. You are using a thirty-two bit operating system</source>
        <translation>. Du använder ett tretiotvåbitars operativsystem</translation>
    </message>
    <message>
        <location filename="../available.cpp" line="120"/>
        <source>Except that this program may crash if you touch the volume control.</source>
        <translation>Förutom att det här programmet kan krascha om du ändrar på volymkontrollen.</translation>
    </message>
    <message>
        <location filename="../available.cpp" line="125"/>
        <source>. It should work fine. </source>
        <translation>. det borde fungera fint. </translation>
    </message>
    <message>
        <location filename="../available.cpp" line="125"/>
        <source> I am licensed with General Public License version 3. You have full access to mine source code. I have been created using the application framework cute version </source>
        <translation> Jag är licensierad med General Public License version 3. Du har full tillgång till min källkod. Jag har skapats med hälp av applikationsramverket qt version </translation>
    </message>
    <message>
        <location filename="../available.cpp" line="125"/>
        <source>I&apos;m going to do my very best. Please write or paste the text you want me to read aloud. You can also drag and drop a text file to me. Then I will read it to you. I use the operating system technology for synthetic speech. I see you are using the operating system </source>
        <translation>Jag ska göra mitt allra bästa. Skriv eller klistra in texten du vill att jag ska läsa upp. Du kan också dra och släppa en textfil till mig. Då ska jag läsa den för dig. Jag använder operativsystemets tekniken för syntetiskt tal. Jag ser att du använder operativsystemet </translation>
    </message>
    <message>
        <source> I am licensed with General Public License version 3. You have full access to mine source code.</source>
        <translation type="vanished"> Jag är licensierad med General Public License version 3. Du har full tillgång till min källkod.</translation>
    </message>
    <message>
        <location filename="../check_for_updates.cpp" line="25"/>
        <location filename="../check_for_updates.cpp" line="37"/>
        <source>Please click on &quot;Update&quot;</source>
        <translation>Vänligen klicka på &quot;Uppdatera&quot;</translation>
    </message>
    <message>
        <location filename="../setstartconfig.cpp" line="79"/>
        <source>Paste text here or drag and drop a text file.
Right-click on the program to access more options.</source>
        <translation>Klistra in text här eller dra och släpp en textfil.
Högerklicka på programmet för att komma åt fler alternativ.</translation>
    </message>
</context>
</TS>

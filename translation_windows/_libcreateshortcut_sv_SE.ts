<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>Createshortcut</name>
    <message>
        <location filename="../createshortcut.cpp" line="35"/>
        <location filename="../createshortcut.cpp" line="51"/>
        <source>Shortcut could not be created in</source>
        <translation>Genvägen kunde inte skapas i</translation>
    </message>
    <message>
        <location filename="../createshortcut.cpp" line="36"/>
        <location filename="../createshortcut.cpp" line="52"/>
        <location filename="../removeshortcut.cpp" line="34"/>
        <location filename="../removeshortcut.cpp" line="48"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../removeshortcut.cpp" line="33"/>
        <location filename="../removeshortcut.cpp" line="47"/>
        <source>The shortcut could not be removed:</source>
        <translation>Genvägen kunde inte tas bort:</translation>
    </message>
</context>
</TS>

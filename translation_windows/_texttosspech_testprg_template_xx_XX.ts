<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="55"/>
        <source>Engines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="90"/>
        <source>Locales</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="125"/>
        <source>Voices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="158"/>
        <source>Solution for Ubuntu. Possibly also for other Linux distributions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="171"/>
        <source>Speak at drag and drop.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="206"/>
        <source>Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="250"/>
        <source>Pitch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="291"/>
        <source>Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="348"/>
        <source>Speak!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="373"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="398"/>
        <source>Resume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="423"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="448"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="473"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="498"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="529"/>
        <source>State:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="38"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="42"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="45"/>
        <source>Select language file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="48"/>
        <source>Check for updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="52"/>
        <source>Check for updates at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="67"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="71"/>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="77"/>
        <source>Desktop Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="92"/>
        <source>Applications menu Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="158"/>
        <source>Open Language file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="158"/>
        <source>Language Files (*.qm)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="334"/>
        <source>Goodbye!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="344"/>
        <source>My name is </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="376"/>
        <location filename="../setstartconfig.cpp" line="40"/>
        <source>Rate: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="382"/>
        <location filename="../setstartconfig.cpp" line="43"/>
        <source>Pitch: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="431"/>
        <source>State: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../available.cpp" line="114"/>
        <source>. You are using a sixty-four bit operating system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../available.cpp" line="116"/>
        <source>. You are using a thirty-two bit operating system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../available.cpp" line="120"/>
        <source>Except that this program may crash if you touch the volume control.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../available.cpp" line="125"/>
        <source>. It should work fine. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../available.cpp" line="125"/>
        <source> I am licensed with General Public License version 3. You have full access to mine source code. I have been created using the application framework cute version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../available.cpp" line="125"/>
        <source>I&apos;m going to do my very best. Please write or paste the text you want me to read aloud. You can also drag and drop a text file to me. Then I will read it to you. I use the operating system technology for synthetic speech. I see you are using the operating system </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="388"/>
        <location filename="../setstartconfig.cpp" line="32"/>
        <source>Volume: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_for_updates.cpp" line="25"/>
        <location filename="../check_for_updates.cpp" line="37"/>
        <source>Please click on &quot;Update&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setstartconfig.cpp" line="79"/>
        <source>Paste text here or drag and drop a text file.
Right-click on the program to access more options.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
